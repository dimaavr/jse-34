package ru.tsc.avramenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArg();

    @NotNull
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    AbstractCommand getCommandByArg(@Nullable String arg);

    void add(@NotNull AbstractCommand command);

}
