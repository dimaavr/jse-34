package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.api.repository.IOwnerRepository;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.AbstractOwnerEntity;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @NotNull
    @Override
    public E add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        final List<E> listEnt = findAll(userId);
        list.remove(listEnt);
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    public void clear(@NotNull final String userId) {
        final List<String> listEnt = list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .map(E::getId)
                .collect(Collectors.toList());
        listEnt.forEach(list::remove);
    }

    @NotNull
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .filter(e -> e.getUserId().equals(userId))
                .findFirst()
                .orElseThrow(ProcessException::new);
    }

    @NotNull
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull
        final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(ProcessException::new);
    }

}