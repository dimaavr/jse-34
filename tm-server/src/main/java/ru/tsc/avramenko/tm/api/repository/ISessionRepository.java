package ru.tsc.avramenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.model.Session;

public interface ISessionRepository extends IOwnerRepository<Session> {

    boolean contains(@NotNull String id);

}